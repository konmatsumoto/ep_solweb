<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
    <title>Netbanking</title>
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script>
</head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
body { background:#171717; }
div#menu {
    margin:30px auto;
    width:80%;
}
div#copyright {
    margin:0 auto;
    width:80%;
    font:11px 'Trebuchet MS';
    color:#124a6f;
    text-indent:20px;
    padding:40px 0 0 0;
}
div#copyright a { color:#4682b4; }
div#copyright a:hover { color:#124a6f; }
</style>

<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img width="100%" src="banner.jpg"/></td>
  </tr>
  </table>

<div id="menu">
    <ul class="menu">
        <li class="last"><a href="index.php"><span>Home</span></a></li>
        <li><a href="login.php" class="parent"><span>Consultar Sua Conta</span></a></li>
        <li><a href="seguranca.php" class="parent"><span>Pol�tica de Seguran�a</span></a>
        </li>
    </ul>
</div>

<table width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td id="cabeca" align="left"><strong>Pol�tica de Privacidade: </strong></td>
      </tr>
</table>

<br>
<h2>Seguran�a e Armazenamento da informa��o </h2>
<br><br>
<p id ="texto">O Sistema de NetBanking est� obrigado a observar todas as normas aplic�veis em mat�ria de medidas de seguran�a de Informa��o Pessoal.</p><br>
<p id ="texto">O Sistema de NetBanking considera os dados de seus usu�rios como um bem precioso que deve ser protegido de qualquer perda ou acesso n�o autorizado.</p><br>
<p id ="texto">Emprega, portanto, diversas t�cnicas de seguran�a para proteger tais dados de acessos n�o autorizados por usu�rios de dentro ou fora da empresa.</p><br>
<p id ="texto">Ainda assim, � necess�rio considerar que a seguran�a perfeita n�o existe na Internet.</p><br>
<p id ="texto">Portanto, o Sistema de NetBanking n�o ser� respons�vel por intercepta��es ilegais ou viola��o de seus sistemas ou bases de dados por parte de pessoas n�o autorizadas.</p><br>
<p id ="texto">Tampouco se responsabilizar� pela indevida utiliza��o da informa��o obtida por esses meios.</p><br>



<br>
<br>





<div id="copyright"><p>Netbanking SA &copy; 2014 Todos os direitos reservados</p>
</div>
</body>
</html>
