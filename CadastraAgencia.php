<?php session_start();?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
    <title>Netbanking</title>
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script>

    
</head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
body { background:#171717; }
div#menu {
    margin:30px auto;
    width:80%;
}
div#copyright {
    margin:0 auto;
    width:80%;
    font:11px 'Trebuchet MS';
    color:#124a6f;
    text-indent:20px;
    padding:40px 0 0 0;
}
div#copyright a { color:#4682b4; }
div#copyright a:hover { color:#124a6f; }
</style>



<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img width="100%" src="banner.jpg"/></td>
  </tr>
  </table>

<div id="menu">
    <ul class="menu">
        <li class="last"><a href="index.php"><span>Home</span></a></li>
        <li><a href="admin.php" class="parent"><span>Administra��o</span></a></li>
        <li><a href="seguranca.php" class="parent"><span>Pol�tica de Seguran�a</span></a>
        </li>
       <li><a href="logout.php"><span>Sair</span></a></li>
    </ul>
</div>

<br><br>

<script language="javascript">
function senha() {
	if (document.getElementById('s1').value!=document.getElementById('s2').value) {
	   alert("Digite as senhas corretamente!");
		 return false;
	}
}
</script>

<script language='JavaScript'>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;
    if((tecla > 47 && tecla < 58)) return true;
    else{
    if (tecla != 8) return false;
    else return true;
    }
}
</script>
<script type="text/javascript">
<!--

function main()
{
  document.getElementById("agencia").value = "111";
  var e = document.getElementById("cidade");
  var strUser = e.options[e.selectedIndex].text;
  alert(strUser);
}

// -->
</script>


    <table width="100%" cellspacing="0" cellpadding="0">
	      <tr>
	        <td id="cabeca" align="center"><strong>Cadastro de ag�ncias</strong></td>
	      </tr>
</table>

			<div align="center">
			<h2>Preencha os dados do formul�rio abaixo para  cadastrar uma agencia no Net Banking:</h2>
			</div>

	<form id="formCadastro" onsubmit="return checa(this);" action="cadastrouAgencia.php" method="POST">


    <br>
    <br>
	<fieldset>
				<legend id='fundo'>Inclus�o de ag�ncia Bancaria</legend>
				<table>
					<tr>
			 	<td>
			 		 <label for="agencia" id='fundo'>C�digo de ag�ncia Banc�ria:</label>
				</td>
				<td>
					 <input size="5" maxlength="5" required type="text" onblur="main();" name="agencia" />
				</td>
			</tr>
            <tr> 
            <td>
            <label for="estado" id='fundo'>Estado:</label>
            </td>
            <td>
              <select name="estado" id="estado">
                 <option value="">Selecione um estado</option>
                 <?php
                   include("conexao.php"); 
                   $sql = "SELECT id, uf, nome FROM netbanking.estados ORDER BY uf";
	               $res = mysql_query( $sql );
	               while ( $row = mysql_fetch_assoc( $res ) ) {
    	                   echo '<option value="'.$row['id'].'">'.$row['nome'].'</option>';
	               } 
                 ?>
              </select>
            </td>
            </tr>
			<tr>
			<td>
			<label for="cidade" id='fundo'>Cidade:</label>
				</td>
				<td>
			   	<select name="cidade" id="cidade">
                 <option value=""> Escolha um estado para selecionar a cidade</option>
                 </select>
                 <script src="http://www.google.com/jsapi"></script>
                 <script type="text/javascript"> google.load('jquery', '1.3');	</script>
                 <script type="text/javascript">
                   $(function(){
                     $('#estado').change(function(){
    	               if($(this).val() ) {
      		             $('#cidade').hide();
			             $('.carregando').show();
			             $.getJSON('cidades.ajax.php?search=',{estado: $(this).val(), ajax: 'true'},
			             function(j){
			             var options = '<option value="">Escolha uma cidade</option>';
    			         for (var i = 0; i < j.length; i++) {							options += '<option value="' + j[i].cidade + '">' + j[i].nome + '</option>';				
				         }
				         $('#cidade').html(options).show();
				         $('.carregando').hide();
			             });
		                }
		                else {	
     		              $('#cidade').html('<option value="">- Escolha um estado -</option>');
		                }
	                 });
	                });
                </script>
				</td>
			</tr>
			<tr>
			 <td>
			   <label for="bairro" id='fundo'>Bairro:</label>
			</td>
			<td>
			   <input size="30" maxlength="25" required type="text" name="bairro" />
			</td>
			</tr>
			<tr>
			 <td>
			   <label for="rua" id='fundo'>Logradouro:</label>
			</td>
			<td>
			   <input size="30" maxlength="30" required type="text" name="rua" />
			</td>
			</tr>
			<tr>
			<td>
			   <label for="nEndereco" id='fundo'>
				 N�mero do Endere�o:</label>
			</td>
			<td>
			<input size="3" maxlength="3" required onkeypress='return SomenteNumero(event)'
			  	 type="text" name="nEndereco" />
			</td>
			</tr>

		</table>

	</fieldset>


  <br>
		<div align="center">
		<input type="submit" value="Cadastrar" name="cadastrar" onClick="main();"/>
		</div>
  </form>

  <div id="copyright"><p>Netbanking SA &copy; 2014 Todos os direitos reservados </p></div>

</body>
</html>
