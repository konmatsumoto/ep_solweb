<?php session_start();?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
    <title>Netbanking</title>
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" href="style.css" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script>
    <script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/s3Slider.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#banner").s3Slider({
			timeOut: 4000 // Tempo de transição de cada banner.
		});
	});
</script>
</head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
body { background:#171717; }
div#menu {
    margin:30px auto;
    width:80%;
}
div#copyright {
    margin:0 auto;
    width:80%;
    font:11px 'Trebuchet MS';
    color:#124a6f;
    text-indent:20px;
    padding:40px 0 0 0;
}
div#copyright a { color:#4682b4; }
div#copyright a:hover { color:#124a6f; }
</style>

<?php
   if(isSet($_SESSION['IDfunc']))
   {
      echo "<script> top.location.href='admAutenticacao.php'; </script>;";
   }
   else {
      if(isSet($_SESSION['CodConta'])) {
	    echo "<script> top.location.href='autentica.php'; </script>;"; 
	  }
   }
?>

<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img width="100%" src="banner.jpg"/></td>
  </tr>
  </table>

<div id="menu">
    <ul class="menu">
        <li class="last"><a href="index.php"><span>Home</span></a></li>
        <li><a href="login.php" class="parent"><span>Consultar Sua Conta</span></a></li>
        <li><a href="seguranca.php" class="parent"><span>Pol�tica de Seguran�a</span></a>
        </li>
    </ul>
</div>
<table>
<tr>
<td>
<h1 id = "cabeca"> Bem Vindo ao Sistema de Netbanking! </h1>
<div id="container">
	<div id="banner">
        <ul id="bannerContent">
            <li class="bannerImage">
                <img src="images/Google.jpg" />
                <span class="left">
                	<strong>Google</strong>
                    <br /><a id = "voltar" href = "http://www.google.com">O maior buscador do mundo ajuda na divulga��o do site e isso nos ajudou muito em nosso crescimento na �ltima d�cada.
                </a></span>
            </li>
            <li class="bannerImage">
                <img src="images/interrogacao.jpg" />
                <span class="right">
                	<strong>Com d�vidas sobre o nosso site</strong>
                    <br /><a id = "voltar" href = "entrevista.php">Clique aqui para ler uma entrevista com o nosso cliente mais antigo...
                </a><br><a id = "voltar" href = "ajuda.php">ou se prefirir visualize a nossa p�gina de ajuda
                </a></span>
            </li>
            <li class="bannerImage">
                <img src="images/wide/3.jpg" />
                <span class="left">
                	<strong>Title text 3</strong>
                    <br />There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form...
                </span>
            </li>
            <li class="bannerImage">
                <img src="images/wide/4.jpg" />
                <span class="right">
                	<strong>Title text 4</strong>
                    <br />There are many variations of passages of Lorem Ipsum available...
                </span>
            </li>
            <li class="bannerImage">
                <img src="images/wide/5.jpg" />
                <span class="right">
                	<strong>Title text 5</strong>
                    <br />There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form...
                </span>
            </li>
        </ul>
    </div>
</div>
</td>
<td><br>
</td></tr></table>
<br> <br><br>

<div id="copyright"><p>Netbanking SA &copy; 2014 Todos os direitos reservados 
<a id="copyright" href="admin.php">  Administra��o</a></p>
</div>

</body>
</html>