<?php session_start();?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
    <title>Netbanking</title>
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript" src="menu.js"></script>
</head>
<body>

<style type="text/css">
* { margin:0;
    padding:0;
}
body { background:#171717; }
div#menu {
    margin:30px auto;
    width:80%;
}
div#copyright {
    margin:0 auto;
    width:80%;
    font:11px 'Trebuchet MS';
    color:#124a6f;
    text-indent:20px;
    padding:40px 0 0 0;
}
div#copyright a { color:#4682b4; }
div#copyright a:hover { color:#124a6f; }
</style>

<?php
   if(isSet($_SESSION['IDfunc']))
   {
      echo "<script> top.location.href='admAutenticacao.php'; </script>;";
   }
   else {
      if(!isSet($_SESSION['CodConta'])) {
	    echo "<script> top.location.href='login.php'; </script>;"; 
	  }
   }
?>

<script language='JavaScript'>
function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;
    if((tecla > 47 && tecla < 58)) return true;
    else{
    if (tecla != 8) return false;
    else return true;
    }
}
</script>

<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><img width="100%" src="banner.jpg"/></td>
  </tr>
  </table>

<div id="menu">
    <ul class="menu">
        <li class="last"><a href="index.php"><span>Home</span></a></li>
        <li><a href="login.php" class="parent"><span>Consultar Sua Conta</span></a></li>
        <li><a href="seguranca.php" class="parent"><span>Pol�tica de Seguran�a</span></a>
        </li>
		<li><a href="logout.php"><span>Sair</span></a></li>
    </ul>
</div>

<br><br>

<table width="100%" cellspacing="0" cellpadding="0">
	      <tr>
	        <td id="cabeca" align="center"><strong>Transa��o online</strong></td>
	      </tr>
</table>


			

	<form id="formCadastro" onsubmit="return checa(this);" action="fazerTransacao.php" method="POST">

			<fieldset>
			<legend id='fundo'>Conta Corrente</legend>
			<table>
				<tr>
		 	<td>
		 		 <label for="Conta" id='fundo'>Conta Corrente:</label>
			</td>
			<td>
				 <input size="15"  type="text" name="conta" readonly="true" value="<?php
				    echo $_SESSION['CodConta'];
				 
				 ?>"/>
			</td>
		</tr>
	</table>
    </fieldset>
    <br>
    <br>
	<fieldset>
				<legend id='fundo'>Ag�ncia Bancaria</legend>
				<table>
					<tr>
			 	<td>
			 		 <label for="agencia" id='fundo'>Ag�ncia Banc�ria:</label>
				</td>
				<td>
					 <input size="15"  type="text" name="agencia" readonly="true" value="<?php
					    echo $_SESSION['nAgencia'];					 
					 
					 ?>"/>
				</td>
			</tr>
	</table>

	</fieldset>
	<br>
	<br>
	<fieldset>
	<legend id='fundo'>Conta Destino</legend>
	<table>
	<tr>
	<td>
	<label for="contaDestino" id='fundo'> Conta de destino: </label>
	</td>
	<td>
	<Input required maxlength = 6 type="text" name="contaDestino" id="s1" />
	</td>
	</tr>
	<tr>
	<td>
	<label for="valor1" id='fundo'> Valor: </label>
	</td>
	<td>
	<Input  required maxlength = 8 type="text" name="valor1" onkeypress='return SomenteNumero(event)'/>
	</td><td><label for="valor2" id='fundo'>,</label></td.<td>
	<Input  required size = 2 maxlength = 2 type="text" name="valor2" onkeypress='return SomenteNumero(event)'/>
	</td>
	</tr>
	</table>
	</fieldset>

  <br>
		<div align="center">
		<input type="submit" value="Concluir" name="concluir" />
		</div>
  </form>



<br><br><br><br>



<div id="copyright"><p>Netbanking SA &copy; 2014 Todos os direitos reservados</p>
</div>

</body>
</html>